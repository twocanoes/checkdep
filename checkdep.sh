#!/bin/bash 
usage() { 
	echo "Usage: $0 [-v] [-n <vm_hostname>] [-f <path_to_vmx_file>] [-r <path_to_vm_run_command>] -m <path_to_vm> <serial numbers>" 1>&2; exit 1; 
}

echo_verbose() {
	
	if [ $verbose -eq 1 ]; then
		echo $1 > /dev/stderr
	fi
		
}

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
	echo "** Trapped CTRL-C"
	echo shutting down vm. be patient.
	"$vm_run"  stop "$vm_file" hard 
	exit 0
	
}


vm_hostname=catalinavm.local
vm_run="/Applications/VMware Fusion.app/Contents/Public/vmrun"
vmx_file=""
vm_file=""
verbose=0

while getopts "vf:r:m:n:" o; do
	case "${o}" in

		v)
			verbose=1
		;;
		n)
			vm_hostname="${OPTARG}"
		;;
		f)
			vmx_file="${OPTARG}"
		;;
		r)
			vm_run="${OPTARG}"
		;;
		m)
			vm_file="${OPTARG}"
		;;

		*)
			usage
		;;
		esac
	done
shift $((OPTIND-1))

if  [ -z "${vm_file}" ] ; then
	usage
	exit -1
fi


if [ -z "${vmx_file}" ]; then
	vmx_file=`echo "$vm_file"/*.vmx`
fi

dep_serials="$@"


if  [ -z "${dep_serials}" ] ; then
	usage
	exit -1
fi

echo
echo "searching for DEP records $dep_serials"
echo "------------------"


for serial in $dep_serials; do  

	echo_verbose "stopping virtual machine in case it is running" 
			
			
	"$vm_run" stop "$vm_file" hard 1>&2 > /dev/null
	if [ $verbose -eq 1 ]; then
			
		echo "setting serial number for virtual machine"
	fi
	
	grep -v serialNumber "${vmx_file}"  2>&1  > "${vmx_file}_new" 
	cp "${vmx_file}_new"  "${vmx_file}"
	
	echo serialNumber = \"$serial\" >> "${vmx_file}"
	
	echo_verbose "Starting virtual machine" 
	
		
	"$vm_run" start "$vm_file" nogui 2>/dev/null 1>/dev/null
	
	if [ $? -ne 0 ]; then
		echo starting up vm with vm_run
	fi
	
	echo_verbose "running command via ssh"

	tries=5
	found=0
	
	while [ $tries -gt 0 ] ; do

		ping -c 1 -q $vm_hostname 2>&1 2> /dev/null 1>/dev/null
		if [ $? -eq 0 ]; then
			found=1;
			break;
		fi
		tries=$(($tries-1))

	done
	if [ $found -eq 0 ]; then
		echo could not connect to $vm_hostname
		exit -1
	fi

	result=$(ssh root@$vm_hostname "profiles show -type enrollment"  2>&1  | grep -v Warning)
	
	
	if [[ $result =~ .*"URL".* ]]; then  
		echo "$serial Device Enrollment configuration" 
		echo $result
	elif [[ $result =~ .*"Device Enrollment configuration".* ]]; then  
		echo "$serial no DEP record found" 
	else
		echo "$serial error getting serial" 
		exit -1
		
	fi
	echo ------------------


	echo_verbose "stopping virtual machine"
	
	"$vm_run"  stop "$vm_file" hard > /dev/stderr

done

		
